<?php
/**
 * SelectQuestionByLdap get list of answers in LDAP
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2022-2023 Denis Chenu <www.sondages.pro>
 * @copyright 2022 OECD (Organisation for Economic Co-operation and Development ) <www.oecd.org>
 * @license GPL v3
 * @version 0.2.0
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU AFFERO GENERAL PUBLIC LICENSE as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
class SelectQuestionByLdap extends PluginBase
{

    static protected $description = 'Use a text question as a dropdown filled by LDAP request';
    static protected $name = 'SelectQuestionByLdap';

    protected $storage = 'DbStorage';

    /** @inheritdoc, this plugin didn't have any public method */
    public $allowedPublicMethods = array();

    protected $settings = array(
        'server' => array(
            'type' => 'string',
            'label' => 'LDAP server',
            'help' => 'e.g. ldap://ldap.example.com or ldaps://ldap.example.com'
        ),
        'ldapport' => array(
            'type' => 'string',
            'label' => 'Port number',
            'help' => 'Default when omitted is 389',
        ),
        'ldapversion' => array(
            'type' => 'select',
            'label' => 'LDAP version',
            'options' => array('2' => 'LDAPv2', '3'  => 'LDAPv3'),
            'default' => '2',
        ),
        'ldapoptreferrals' => array(
            'type' => 'boolean',
            'label' => 'Select true if referrals must be followed (use false for ActiveDirectory)',
            'default' => '0'
        ),
        'ldaptls' => array(
            'type' => 'boolean',
            'help' => 'Check to enable Start-TLS encryption, when using LDAPv3',
            'label' => 'Enable Start-TLS',
            'default' => '0'
            ),
        'binddn' => array(
            'type' => 'string',
            'label' => 'DN of the LDAP account used to search for the end-user\'s DN.',
            'help' => 'Optional , an anonymous bind is performed if empty',

        ),
        'bindpwd' => array(
            'type' => 'password',
            'label' => 'Password of the LDAP account used to search for the end-user\'s DN.'
        ),

        'searchattribute' => array(
            'type' => 'string',
            'label' => 'Default attribute to use for search',
            'help' => '',
        ),
        'searchbase' => array(
            'type' => 'text',
            'label' => 'Default Base DN for the search operation',
            'help' => 'Multiple bases may be separated by a new line',
        ),
        'searchfilter' => array(
            'type' => 'string',
            'label' => 'Default Optional extra LDAP filter',
            'help' => 'Don\'t forget the outmost enclosing parentheses',
        ),
    );

    /**
    * Add function to be used in beforeQuestionRender event and to attriubute
    */
    public function init()
    {
        $this->subscribe('beforeQuestionRender');
        $this->subscribe('newQuestionAttributes');
        /* Return the json */
        $this->subscribe('newDirectRequest');
    }

    /**
    * The attribute
    */
    public function newQuestionAttributes()
    {
        $newAttributes = array(
            'selectQuestionByLdapActive'=>array(
                'types' => 'Q',
                'category' => $this->gT('Ldap'),
                'sortorder' => 100,
                'inputtype' => 'switch',
                'default' => 0,
                'help' => $this->gT('The question is set as LDAP search, the 1st subquestions is used to save DN. The 1st subquestions is used to save DN value, no control on name is done.'),
                'caption' => $this->gT('Ldap search'),
            ),
            'selectQuestionByLdapAttribute'=>array(
                'types' => 'Q',
                'category' => $this->gT('Ldap'),
                'sortorder' => 200,
                'inputtype' => 'text',
                'default' => "",
                'help' => $this->gT('Filter was done using this attribute, and is used to show as user for the list too.'),
                'caption' => $this->gT('Attribute to search'),
            ),
            'selectQuestionByLdapSearchbase'=>array(
                'types' => 'Q',
                'category' => $this->gT('Ldap'),
                'sortorder' => 210,
                'inputtype' => 'textarea',
                'default' => "",
                'help' => $this->gT('Leave empty if you want to use default.'),
                'caption' => $this->gT('Base DN for the search operation'),
            ),
            'selectQuestionByLdapSearchfilter'=>array(
                'types' => 'Q',
                'category' => $this->gT('Ldap'),
                'sortorder' => 220,
                'inputtype' => 'text',
                'default' => "",
                'help' => $this->gT('Leave empty if you want to use default.'),
                'caption' => $this->gT('Optional extra LDAP filter'),
            ),
            'selectQuestionByLdapMultiple'=>array(
                'types' => 'Q',
                'category' => $this->gT('Ldap'),
                'sortorder' => 230,
                'inputtype' => 'switch',
                'default' => 0,
                'help' => $this->gT('Set select as multiple'),
                'caption' => $this->gT('Allow multiple selection'),
            ),
            'selectQuestionByLdapMultipleSeparator'=>array(
                'types' => 'Q',
                'category' => $this->gT('Ldap'),
                'sortorder' => 240,
                'inputtype' => 'text',
                'default' => ";",
                'help' => $this->gT('Incase of multiple choice, use this caracter as multiple choice. default is <code>;</code> , reset to default if length of string is differnt than 1'),
                'caption' => $this->gT('Separator for multiple choice'),
            ),
            'selectQuestionByLdapValueSeparator'=>array(
                'types' => 'Q',
                'category' => $this->gT('Ldap'),
                'sortorder' => 250,
                'inputtype' => 'text',
                'default' => ",",
                'help' => $this->gT('Use <code>,</code> as default'),
                'caption' => $this->gT('Separator when multiple values are found for an attribute are found'),
            ),
            'selectQuestionByLdapPlaceholder'=>array(
                'types' => 'Q',
                'i18n' => true,
                'category' => $this->gT('Ldap'),
                'sortorder' => 260,
                'inputtype' => 'text',
                'default' => "",
                'help' => $this->gT('The place holder, default to translated «Search»'),
                'caption' => $this->gT('Place holder on the input'),
            ),
        );
        $this->getEvent()->append('questionAttributes', $newAttributes);
    }

    /**
    * Update Answer part in question display
    */
    public function beforeQuestionRender()
    {
        $questionRenderEvent = $this->getEvent();
        $qid = $questionRenderEvent->get('qid');

        $aAttributes = QuestionAttribute::model()->getQuestionAttributes($qid);
        if (empty($aAttributes['selectQuestionByLdapActive'])) {
            return;
        }
        $baseSgq = $questionRenderEvent->get('surveyId') . "X" . $questionRenderEvent->get('gid') . "X" . $qid;
        $answer = $this->renderAnswer(
            $questionRenderEvent->get('surveyId'),
            $qid,
            $baseSgq,
            $aAttributes,
            $questionRenderEvent->get("answers")
        );
        $vclass = "selectquestionbyldap";
        $vtip = ""; //$this->gT("TEST"); // TODO
        $validMessage = App()->twigRenderer->renderPartial(
            '/survey/questions/question_help/em_tip.twig',
            [
                'qid'       => $qid,
                'coreId'    => "vmsg_{$qid}_{$vclass}",
                'coreClass' => "ls-em-tip em_{$vclass}",
                'vclass'    => $vclass,
                'vtip'      => $vtip,
                'hideTip'   => boolval($aAttributes['hide_tip']),
                'qInfo'     => array(),
            ]
        );
        $this->initJavascript();
        $questionRenderEvent->set("answers", $answer);
        $questionRenderEvent->set("valid_message", $questionRenderEvent->get("valid_message") . $validMessage);
        $questionRenderEvent->set("class", $questionRenderEvent->get("class") . " selectquestionbyldap-question");
        
    }

    public function newDirectRequest()
    {
        if($this->getEvent()->get('target') != get_class($this)) {
            return;
        }
        $qid = App()->getRequest()->getQuery('qid');
        if (empty($qid)) {
            throw new CHttpException(400, gT("Invalid parameters."));
        }
        $oQuestion = Question::model()->findByPk($qid);
        if(empty($oQuestion)) {
            throw new CHttpException(404, gT("Question not found."));
        }
        $aAttributes = QuestionAttribute::model()->getQuestionAttributes($qid);
        if (empty($aAttributes['selectQuestionByLdapActive'])) {
            throw new CHttpException(400, gT("Invalid question."));
        }
        if (empty($_SESSION['survey_' . $oQuestion->sid])) {
            throw new CHttpException(403, gT("Survey not started."));
        }
        $term = App()->getRequest()->getQuery('term');
        Yii::setPathOfAlias(get_class($this), dirname(__FILE__));
        $settings = $this->getLdapSettingsByQid($aAttributes);
        $options = array();
        $subQuestions = $oQuestion->subquestions;
        array_shift($subQuestions);
        $extraAttributes = array();
        foreach ($subQuestions as $subQuestion) {
            $extraAttributes[] = $subQuestion->title;
        }
        $options['extraAttributes'] = $extraAttributes;
        $options['valueseparator'] = $aAttributes['selectQuestionByLdapValueSeparator'];
        if(empty($options['valueseparator'])) {
            $options['valueseparator'] = ",";
        }
        Yii::setPathOfAlias(get_class($this), dirname(__FILE__));
        $LdapFilterToJson = new SelectQuestionByLdap\LdapFilterToJson(
            $settings,
            $options
        );
        $result = $LdapFilterToJson->getFilteredResult($term);
        if (empty($result)) {
            $error = $LdapFilterToJson->ErrorInfo;
            if($error) {
                throw new CHttpException(500, $error);
            }
        }
        $this->myRenderJson([
            'results' => array_values($result)
        ]);
    }

    /**
     * When need update answers part
     */
    public function addTwigPath()
    {
        $viewPath = dirname(__FILE__)."/twig";
        $this->getEvent()->append('add', array($viewPath));
    }

    private function initJavascript()
    {
        Yii::setPathOfAlias(get_class($this),dirname(__FILE__));
        $min = (App()->getConfig('debug')) ? '.min' : '';
        if(!Yii::app()->clientScript->hasPackage('limesurvey-selectquestionbyldap')) {
            Yii::app()->clientScript->addPackage('limesurvey-selectquestionbyldap', array(
                'basePath'    => get_class($this).'.assets',
                'js'          => array('selectquestionbyldap.js'),
                'css'         => array('selectquestionbyldap.css'),
                'depends'     => array('bootstrap-select2'),
            ));
        }
        /* Registering the package */
        Yii::app()->getClientScript()->registerPackage('limesurvey-selectquestionbyldap');
    }

    private function renderAnswer($sid, $qid, $baseSgq, $aQuestionAttributes, $answers)
    {
        $this->subscribe('getPluginTwigPath', 'addTwigPath');
        $language = App()->getLanguage();
        $oQuestion = Question::model()->findByPk($qid);
        if (empty($oQuestion->subquestions)) {
            return $answers;
        }
        /* Fix some attribute */ 
        $aQuestionAttributes['selectQuestionByLdapValueSeparator'] = trim($aQuestionAttributes['selectQuestionByLdapValueSeparator']);
        if (strlen($aQuestionAttributes['selectQuestionByLdapValueSeparator']) != 1) {
            $aQuestionAttributes['selectQuestionByLdapValueSeparator'] = ";";
        }
        $subQuestions = $oQuestion->subquestions;
        $dnQuestionFirst = array_shift($subQuestions);
        /* Get current value if exist */
        $currentOptions = array();
        $prefillDatas = array();
        if(!empty($_SESSION["survey_$sid"][$baseSgq . $dnQuestionFirst->title])) {
            $extraAttributes = array();
            foreach ($subQuestions as $subQuestion) {
                $extraAttributes[] = $subQuestion->title;
            }
            $options['extraAttributes'] = $extraAttributes;
            $options['valueseparator'] = $aQuestionAttributes['selectQuestionByLdapValueSeparator'];
            $currentValue = $_SESSION["survey_$sid"][$baseSgq . $dnQuestionFirst->title];
            if ($aQuestionAttributes['selectQuestionByLdapMultiple']) {
                $currentValues = explode($aQuestionAttributes['selectQuestionByLdapMultipleSeparator'], $currentValue);
            } else {
                $currentValues = array($currentValue);
            }
            Yii::setPathOfAlias(get_class($this), dirname(__FILE__));
            $settings = $this->getLdapSettingsByQid($aQuestionAttributes);
            $LdapFilterToJson = new SelectQuestionByLdap\LdapFilterToJson(
                $settings,
                $options
            );
            foreach ($currentValues as $id) {
                $result = $LdapFilterToJson->getValueAndDataFor($id);
                if($result) {
                    $currentOptions[$id] = $result;
                    $prefillData = array(
                        'id' => $id,
                        'text' => $result['text']
                    );
                    foreach($result['data'] as $key => $value) {
                        $prefillData[$key] = $value;
                    }
                    $prefillDatas[] = $prefillData;
                }

            }
        }
        $dnQuestionFirstl10n =  QuestionL10n::model()->find(
            "qid = :qid and language = :language",
            array(':qid' => $dnQuestionFirst->qid, ':language' => $language)
        );
        $placeholder = $this->gT("Search");
        if (!empty($aQuestionAttributes['selectQuestionByLdapPlaceholder'][$language])) {
            $placeholder = $aQuestionAttributes['selectQuestionByLdapPlaceholder'][$language];
        }

        $scriptOptions = array(
            'serviceUrl' => App()->getController()->createUrl(
                'plugins/direct',
                array('plugin' => get_class($this), 'function' => 'getData', 'qid' => $qid)
            ),
            'placeholder' => $placeholder,
            'multiple' => boolval($aQuestionAttributes['selectQuestionByLdapMultiple']),
            'language' => $language, // Need to register language script too, todo : create limesurvey lang to slect2 lang
            'prefillData' => $prefillDatas
        );
        $renderData = array(
            'sid' => $sid,
            'qid' => $qid,
            'baseSgq' => $baseSgq,
            'currentOptions' => $currentOptions,
            'label' => $dnQuestionFirstl10n->question,
            'aQuestionAttributes' => $aQuestionAttributes,
            'scriptOptions' => $scriptOptions,
            'coreanswers' => $answers
        );
        return App()->twigRenderer->renderPartial(
            './survey/questions/answer/multipleshorttext/dropdownldap.twig',
            $renderData
        );
    }

    /**
     * render json
     * @param mixed
     * @return void
     */
    private function myRenderJson($data = null) {
        viewHelper::disableHtmlLogging();
        header('Content-type: application/json; charset=utf-8');
        echo json_encode($data);
        Yii::app()->end();
    }

    /**
     * return the settngs for LDAP by attributes
     * @param string[] $aAttributes
     * return array[]
     */
    private function getLdapSettingsByQid($aAttributes)
    {
        $settings = $this->getPluginSettings(true);
        if (!empty($aAttributes['selectQuestionByLdapAttribute'])) {
            $settings['searchattribute']['current'] = trim($aAttributes['selectQuestionByLdapAttribute']);
        }
        if (!empty($aAttributes['selectQuestionByLdapSearchbase'])) {
            $settings['searchbase']['current'] = trim(\LimeExpressionManager::ProcessStepString($aAttributes['selectQuestionByLdapSearchbase'], 3, true));
        }
        if (!empty($aAttributes['selectQuestionByLdapSearchfilter'])) {
            $settings['searchfilter']['current'] = trim(\LimeExpressionManager::ProcessStepString($aAttributes['selectQuestionByLdapSearchfilter'], 3, true));
        }
        return $settings;
    }
}
