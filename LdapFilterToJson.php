<?php

/**
 * This file is part of SelectQuestionByLdap plugin
 * @version 0.1.0
 */

namespace SelectQuestionByLdap;

class LdapFilterToJson
{

    /**
     * the error when try to authenticate
     * @var string
     **/
    public $ErrorInfo = '';

    /**
     * the settings of pplugi for LDAP connexion
     * @var null|array
     **/
    protected $settings;

    /**
     * the options for this request
     * @var null|array
     **/
    protected $options;

    /**
     * @param array $settings
     * @param array $options
     */
    public function __construct($settings, $options)
    {
        $this->settings = $settings;
        $this->options = $options;
    }

    /**
     * Create connexion and return resource
     */
    private function createConnection()
    {
        $ldapserver = $this->getSetting('server');
        if (strpos($ldapserver, 'ldaps://') === false && strpos($ldapserver, 'ldap://') === false) {
            $ldapserver = 'ldap://' . $ldapserver;
        }
        $ldapport = $this->getSetting('ldapport');
        if (empty($ldapport)) {
            $ldapport = 389;
        }
        $ldapver = $this->getSetting('ldapversion');
        $ldaptls = $this->getSetting('ldaptls');
        $ldapoptreferrals  = $this->getSetting('ldapoptreferrals');
        $ldapconn = ldap_connect($ldapserver . ':' . (int) $ldapport);
        if (false == $ldapconn) {
            // LDAP connect does not connect, but just checks the URI
            $this->ErrorInfo = gT('LDAP URI could not be parsed.');
            return false;
        }
        if (empty($ldapver)) {
            // If the version hasn't been set, default = 2
            $ldapver = 2;
        }

        $connectionSuccessful = ldap_set_option($ldapconn, LDAP_OPT_PROTOCOL_VERSION, $ldapver);
        if (!$connectionSuccessful) {
            $this->ErrorInfo = gT('Error creating LDAP connection');
            return false;
        }
        ldap_set_option($ldapconn, LDAP_OPT_REFERRALS, $ldapoptreferrals);
        if (!empty($ldaptls) && $ldaptls == '1' && $ldapver == 3 && preg_match("/^ldaps:\/\//", $ldapserver) === 0) {
            // starting TLS secure layer
            if (!ldap_start_tls($ldapconn)) {
                ldap_unbind($ldapconn); // Could not properly connect, unbind everything.
                $this->ErrorInfo = gT('Error creating TLS on LDAP connection');
                return false;
            }
        }
        return $ldapconn;
    }

    /**
     * Get filtered result by term
     * @return array[]
     */
    public function getFilteredResult($term)
    {
        if (empty($term)) {
            $this->ErrorInfo = gT('Invalid parameters');
        }
        $ldapconn = $this->createConnection();
        if (!is_resource($ldapconn)) {
            if (empty($this->ErrorInfo)) {
                $this->addError("Unable to create a LDAP connexion.", \CLogger::LEVEL_ERROR);
            }
            return;
        }
        $binddn = $this->getSetting('binddn');
        $bindpwd = $this->getSetting('bindpwd');
        if (empty($binddn)) {
            // There is no account defined to do the LDAP search,
            // let's use anonymous bind instead
            $ldapbindsearch = @ldap_bind($ldapconn);
        } else {
            // An account is defined to do the LDAP search, let's use it
            $ldapbindsearch = @ldap_bind($ldapconn, $binddn, $bindpwd);
        }
        if (!$ldapbindsearch) {
            $this->addError(ldap_error($ldapconn), \CLogger::LEVEL_ERROR);
            ldap_close($ldapconn); // all done? close connection
            return;
        }
        $searchattribute = $this->getSetting('searchattribute');
        $searchbase = $this->getSetting('searchbase');
        $searchfilter = $this->getSetting('searchfilter');
        if ($searchfilter != "") {
            $datasearchfilter = "(&($searchattribute=*$term*)$searchfilter)";
        } else {
            $datasearchfilter = "($searchattribute=*$term*)";
        }
        $searchattributes = array(
            'dn',
            $searchattribute
        );
        $extraAttributes = array();
        if(!empty($this->options['extraAttributes'])) {
            $extraAttributes = $this->options['extraAttributes'];
            $searchattributes = array_merge(
                $searchattributes,
                $extraAttributes
            );
        }
        $dataentries = array();
        $searchbase = preg_split('/\r\n|\r|\n|;/', $searchbase);
        foreach ($searchbase as $base) {
            $dnsearchres = ldap_search($ldapconn, $base, $datasearchfilter, $searchattributes);
            $entries = ldap_get_entries($ldapconn, $dnsearchres);
            foreach($entries as $entry) {
                if(!empty($entry[$searchattribute]) && $entry[$searchattribute]["count"] == 1) {
                    $dataentries[$entry['dn']] = array(
                        'id' => $entry['dn'],
                        'text' => $entry[$searchattribute][0]
                    );
                    foreach($extraAttributes as $extraAttribute) {
                        if(!empty($entry[strtolower($extraAttribute)]) && $entry[strtolower($extraAttribute)]["count"] > 0) {
                            $values = $entry[strtolower($extraAttribute)];
                            unset($values['count']);
                            $dataentries[$entry['dn']][$extraAttribute] = implode($this->options['valueseparator'], $values);
                        } else {
                            $dataentries[$entry['dn']][$extraAttribute] = '';
                        }
                    }
                }
            }
        }
        return $dataentries;
    }

    /**
     * Get the attribute value from dn
     */
    public function getValueAndDataFor($dn)
    {
        $ldapconn = $this->createConnection();
        if (!is_resource($ldapconn)) {
            if (empty($this->ErrorInfo)) {
                $this->addError("Unable to create a LDAP connexion.", \CLogger::LEVEL_ERROR);
            }
            return;
        }
        $binddn = $this->getSetting('binddn');
        $bindpwd = $this->getSetting('bindpwd');
        if (empty($binddn)) {
            // There is no account defined to do the LDAP search,
            // let's use anonymous bind instead
            $ldapbindsearch = @ldap_bind($ldapconn);
        } else {
            // An account is defined to do the LDAP search, let's use it
            $ldapbindsearch = @ldap_bind($ldapconn, $binddn, $bindpwd);
        }
        if (!$ldapbindsearch) {
            $this->addError(ldap_error($ldapconn), \CLogger::LEVEL_ERROR);
            ldap_close($ldapconn); // all done? close connection
            return;
        }
        $searchattribute = $this->getSetting('searchattribute');
        $searchbase = $this->getSetting('searchbase');
        $searchfilter = $this->getSetting('searchfilter');
        $searchattributes = array(
            'dn',
            $searchattribute
        );
        if(!empty($this->options['extraAttributes'])) {
            $extraAttributes = $this->options['extraAttributes'];
            $searchattributes = array_merge(
                $searchattributes,
                $extraAttributes
            );
        }
        $datasearchfilter = "";
        $dnsearchres = ldap_read($ldapconn, $dn, "objectClass=*", $searchattributes);
        $entries = ldap_get_entries($ldapconn, $dnsearchres);
        if($entries && $entries['count'] == 1) {
            $entry = $entries[0];
            if(!empty($entry[strtolower($searchattribute)]) && $entry[strtolower($searchattribute)]["count"] > 0) {
                $result = array(
                    'text' => $entry[strtolower($searchattribute)][0],
                    'data' => array()
                );
                foreach($extraAttributes as $extraAttribute) {
                    if(!empty($entry[strtolower($extraAttribute)]) && $entry[strtolower($extraAttribute)]["count"] > 0) {
                        $values = $entry[strtolower($extraAttribute)];
                        unset($values['count']);
                        $result['data'][$extraAttribute] = implode($this->options['valueseparator'], $values);
                    } else {
                        $result['data'][$extraAttribute] = '';
                    }
                }
                return $result;
            }
        }
        return null;
    }

    /**
     * Get a specified settings from Plugin
     * @param string
     * @return string
     */
    private function getSetting($setting)
    {
        if (empty($this->settings[$setting])) {
            return null;
        }
        if (isset($this->settings[$setting]['current'])) {
            return $this->settings[$setting]['current'];
        }
        if (isset($this->settings[$setting]['default'])) {
            return $this->settings[$setting]['default'];
        }
        return null;
    }
    
    /**
     * set the error when login
     * @param string message
     * @param string level for log
     * @param string function
     * @return void
     */
    private function addError($message, $level = \CLogger::LEVEL_INFO, $function = 'authenticate')
    {
        $this->ErrorInfo = gT($message);
        \Yii::log("Unable to create a LDAP connexion.", $level, 'plugin.LdapTokenAuthenticate.LdapAuthentication.' . $function);
    }
}

