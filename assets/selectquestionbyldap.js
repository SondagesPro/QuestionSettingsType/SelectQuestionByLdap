/**
 * @author Denis Chenu <https://sondages.pro
 */

function SelectQuestionByLdap(sgq, options) {
    if(!$('#ldapautocomplete' + sgq).length) {
        return;
    }
    $('#ldapautocomplete'+sgq).select2({
        ajax: {
            url: options.serviceUrl,
            dataType: 'json',
            delay: 400
        },
        allowClear: true,
        minimumInputLength: 1,
        placeholder: options.placeholder,
        multiple: options.multiple,
        // data: options.prefillData // Don't seems to work
        language: options.language
        
    });
    $('#ldapautocomplete'+sgq).on('select2:select', function (e) {
        let data = e.params.data;
        let basename = $(this).data('basename');
        let destination = $("#" + $(this).data('destination'));
        let dnInput = $(destination).find("[name^='" + basename + "']").first();
        if($(this).prop('multiple')) {
            var multipleseparator = $(this).data('multipleseparator');
            SelectQuestionByLdapMultipleAdd(dnInput, data.id, multipleseparator);
            $.each(data, function( index, value ) {
                if ($("#answer" + basename + index).length) {
                    SelectQuestionByLdapMultipleAdd("#answer" + basename + index, value, multipleseparator);
                }
            });
        } else {
            $(dnInput).val(data.id).trigger("change");
            $.each(data, function( index, value ) {
                if ($("#answer" + basename + index).length) {
                    $("#answer" + basename + index).val(value).trigger("change");
                }
            });
        }
    });
    $('#ldapautocomplete'+sgq).on('select2:unselect', function (e) {
        let data = e.params.data;
        if ($('#ldapautocomplete'+sgq).find("option[value='" + data.id +"']").length) {
            let extradata = $('#ldapautocomplete'+sgq).find("option[value='" + data.id +"']").data();
            $.extend(data, extradata);
            $('#ldapautocomplete'+sgq).find("option[value='" + data.id +"']").remove();
        }
        let basename = $(this).data('basename');
        let destination = $("#" + $(this).data('destination'));
        let dnInput = $(destination).find("[name^='" + basename + "']").first();
        if($(this).attr('multiple')) {
            let multipleseparator = $(this).data('multipleseparator');
            SelectQuestionByLdapMultipleRemove(dnInput, data.id, multipleseparator);
            $.each(data, function( index, value ) {
                if ($("#answer" + basename + index).length) {
                    SelectQuestionByLdapMultipleRemove("#answer" + basename + index, value, multipleseparator);
                }
            });
        } else {
            $(dnInput).val("").trigger("change");
            $.each(data, function( index, value ) {
                if ($("#answer" + basename + index).length) {
                    $("#answer" + basename + index).val("").trigger("change");
                }
            });
        }
    });
    $('#ldapautocomplete'+sgq).on('select2:clear', function (e) {
        let data = e.params.data;
        let basename = $(this).data('basename');
        let destination = $("#" + $(this).data('destination'));
        let dnInput = $(destination).find("[name^='" + basename + "']").first();
        $('#ldapautocomplete'+sgq).find("option").remove();
        $(dnInput).val("").trigger("change");
        $.each(data, function( index, value ) {
            if ($("#answer" + basename + index).length) {
                $("#answer" + basename + index).val("").trigger("change");
            }
        });
    });
}
/**
 * Add an element to an input
 */
function SelectQuestionByLdapMultipleAdd(input, value, separator) {
    let Vals = $(input).val().split(separator);
    Vals = Vals.filter(val=> val != "");
    Vals = Vals.filter(val=> val !== value);
    Vals.push(value);
    $(input).val(Vals.join(separator)).trigger("change");
}
/**
 * Remove an elementt from an input
 */
function SelectQuestionByLdapMultipleRemove(input, value, separator) {
    let Vals = $(input).val().split(separator);
    Vals = Vals.filter(val=> val != "");
    Vals = Vals.filter(val=> val !== value);
    $(input).val(Vals.join(separator)).trigger("change");
}
